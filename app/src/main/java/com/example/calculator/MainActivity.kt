package com.example.calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var resultTextView: TextView
    private var Operand: Double = 0.0
    private var Operator: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultTextView = findViewById(R.id.resultTextView)

    }

    fun numberClick(clickedView: View) {
        if (clickedView is TextView) {
            var result: String = resultTextView.text.toString()
            val number: String = clickedView.text.toString()

            if (result == "0") {
                result == ""
            }

            resultTextView.text = result + number
        }
    }

    fun operationClick(clickedView: View) {
        if (clickedView is TextView) {
            val result: String = resultTextView.text.toString()

            if (result.isNotEmpty()) {
                this.Operand = result.toDouble()
            }
            resultTextView.text = ""
            this.Operator = clickedView.text.toString()
        }

    }

    fun equalsClick(clickedView: View) {
        val Biskvitia2Text: String = resultTextView.text.toString()
        var Biskvitia2: Double = 0.0

        if (Biskvitia2Text.isNotEmpty()) {
            Biskvitia2 = Biskvitia2Text.toDouble()
        }
        var result = ""
        when (Operator) {
            "+" -> result = (Operand + Biskvitia2).toString()
            "-" -> result = (Operand - Biskvitia2).toString()
            "*" -> result = (Operand * Biskvitia2).toString()
            "/" -> result = (Operand / Biskvitia2).toString()
        }
        if (result.endsWith(".0")) {
            result = result.dropLast(2)
        }
        resultTextView.text = result
    }
    fun clearClick(clickedView: View) {
        var clear = ""


        if(clickedView is TextView){
            this.resultTextView.text = clear.toString()
        }
    }
}




